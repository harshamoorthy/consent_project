from django.shortcuts import render

# Create your views here.
from client.models import Client

class Profile(models.Model):
	user = models.OneToOneField(User, related_name='profile')

	client = models.ForeignKey(Client, null=True, related_name='profile')

	class Meta:
		ordering = ['user__id']

	def __unicode__(self):
		return self.user.username
