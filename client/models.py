from django.db import models
from __future__ import unicode_literals
import shortuuid
from django.utils.text import slugify

from clause.models import ClauseBuilder
# Create your models here.

class UUIDMixin(models.Model):
    uuid = models.CharField("UUID", max_length=50, blank=True, null=True, unique=True)

    class Meta:
        abstract = True
class Client(models.Model):
	name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
	sms = models.CharField(max_length=20)
	uuid = models.CharField("UUID", max_length=50, blank=True, null=True, unique=True)
	clause = models.ForeignKey(clause, null=True)

	def save(self, *args, **kwargs):
		if not self.id:
			self.uuid = shortuuid.uuid()


		super(Client, self).save(*args, **kwargs)

	def __unicode__(self):
		return self.name

    def cfjson(self):
        return json.dumps(self.fjson)

    def hdict(self):
        return OrderedDict(sorted({k:ast.literal_eval(v) for k, v in self.data.items()}.items()))

class ClientResponse(models.Model):
    user = models.ForeignKey(User)
    uuid = models.CharField("ShortUUID", max_length=50, blank=True, null=True)
    email = models.EmailField(max_length=100)
	sms = models.CharField(max_length=20)
    clause = models.ForeignKey(clause, null=True)

    def __unicode__(self):
        return self.uuid

    def save(self, *args, **kwargs):
        if not self.id:
            self.uuid = shortuuid.uuid()
        super(ClientResponse, self).save(*args, **kwargs)

    @property
    def cdata(self):
        ddict = {}
        for k, v in self.data.items():
            try:
                ddict[k] = json.loads(v)
            except:
                ddict[k] = v
        return ddict

    @property
    def cdata_res(self):
        ddict = {}
        for k, v in self.data.items():
            if 'basicSelect' in k:
                vdict = json.loads(v)
                if vdict.get('$$hashKey'):
                    del vdict['$$hashKey']
                ddict[str(k)] = {str(k): str(v) for k, v in vdict.items()}
            elif 'groupedSelect' in k:
                nlist = []
                vlist  = json.loads(v)
                for obj in vlist:
                    if obj.get('$$hashKey'):
                        del obj['$$hashKey']
                    nlist.append({str(k): str(v) for k, v in obj.items()})
                print (nlist)
                ddict[str(k)] = nlist
            elif 'checkbox' in k:
                ddict[str(k)] = str(v.lower())
            else:
                try:
                    ddict[str(k)] = json.loads(v)
                except:
                    ddict[str(k)] = str(v)
            print (ddict)
        return ddict
