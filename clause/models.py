from django.db import models
from __future__ import unicode_literals


import shortuuid
from django.contrib.postgres.fields import ArrayField

# Create your models here.


class ClauseBuilder(UUIDMixin, models.Model):
    clauses= ArrayField(models.CharField(max_length=200), blank=True)


    def __unicode__(self):
        return self.clauses
